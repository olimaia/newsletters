---
title: "inktobers e o que mais vem junto"
date: 2020-10-08 11:00:00
---

oi!

resolvi fazer inktober este ano. já que eu estou mesmo nessa pilha de rabiscos e ilustrações e _live streamings_ e por que não?

aí tenho feito _quase_ todo dia uma _live_ do desenho inktober to dia. costuma ser por volta de 14h, no [twitch](https://twitch.tv/shadnyex). pra quem não tem tempo ou saco pra ficar vendo _live_, NÃO TEMA. tem opção pros apressados e ocupados também: vídeos das _lives_ editados em **timelapse**. ou seja: cerca de uma hora de desenho em uns 5-7 minutos de vídeo. altas tecnologias.

os vídeos estou publicado em meu [canal no peertube](https://share.tube/video-channels/art_things) (uma alternativa linda e livre e descentralizada ao youtube). chegue-se :)

contra todas as expectativas tenho também postado os resultados dos inktobers no twitter. tenho entrado lá, digamos, uma vez a cada dois dias. não conta pra ninguém.

logo atualizo também minha página de inktobers do site com as ilustrações deste ano e aviso vocês aqui. os primeiros dias eu publiquei num [post do blog](https://oliviamaia.net/2020/inktobers/).

mas nossa que moça produtiva, você deve estar pensando.

na real, NEM SEI como estou conseguindo fazer essas coisas todas. as _lives_ ajudam muito: é uma forma de não me sentir tão sozinha enquanto trabalho. fica mais divertido. por isso fico aqui chamando vocês pra chegarem junto. é em inglês, eu sei, mas pode falar comigo em português também (óbvio).

e né, ir fazendo um pouquinho por dia.

NÃO É FÁCIL. tô tentano.

aí aproveito de novo pra lembrar da minha nova [página no catarse](https://catarse.me/oliviamaia). porque passou o 5º dia útil do mês e vai que _agora_ dá pra começar a apoiar <3 

também porque se você confirmar assinatura até o dia 10 e optar por uma das opções que recebe ilustração impressa, você recebe a sua primeira ilustração ainda este mês (ou, como comentei no [último post do meu blog](https://oliviamaia.net/2020/catarse/), eu _envio_ ainda este mês e quem sabe chega o mais rápido possível porque correios ainda está um pouco mais lento do que o normal.)

bora dar uma espiadinha na [página](https://catarse.me/oliviamaia) pra conhecer os detalhes e ver onde encontrar tudo que tenho feito. quem sabe você se inspira a dar uma forcinha pra esta artista independente reclusa que se alegra demais em ter companhia pra fazer essas artes todas.

e que mais? como anda essa quarentena pra você? súbito calor da porra? inktober, alguém?

toma aqui dois passarinhos bem abusados comendo todo meu mamão:

![sanhaço-cinzento comento um mamão do pé](https://oliviamaia.net/img/nl/20201007103145.jpg)

![amarelinho mascarado apoiado no mesmo mamão](https://oliviamaia.net/img/nl/20201007103524.jpg)

abraço!

![assinatura](https://oliviamaia.net/img/assinatura.png)  
**olivia maia**  
_escritora e ilustradora desterrada. provavelmente comunista._

→ [olivia@oliviamaia.net](mailto:olivia@oliviamaia.net)  
→ [grupo *rabiscologia* no telegram](https://t.me/joinchat/AisPeBK9IlhYdKVU4fg9Ww)

→ [apoie meu trabalho!](https://catarse.me/oliviamaia)

[mastodon_pt](https://ursal.zone/@olivia) | [mastodon_en](https://radical.town/@olivia)  
[telegram](https://www.t.me/oliviamaia) | [matrix](https://matrix.to/#/@nyex:matrix.org)  
[vídeos](https://share.tube/accounts/olivia/) | [twitch](https://twitch.tv/shadnyex) | [gitlab](https://gitlab.com/oliviamaia)

caixa postal 27  
lençóis - bahia  
46960-000  

---

você recebeu esta mensagem porque seu e-mail está cadastrado em minha newsletter. para **CANCELAR SUA ASSINATURA**, basta responder a este e-mail com seu pedido de cancelamento.

(se você **já fez isso** e eu ainda assim não removi seu e-mail a culpa é toda minha. me avisa que eu juro que resolvo dessa vez.)
