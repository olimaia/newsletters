---
title: ""
date: 2020-11-01 11:00:00
---

TEXTO

![assinatura](https://oliviamaia.net/img/assinatura.png)  
**olivia maia**  
*escritora e ilustradora desterrada.*

→  [olivia@oliviamaia.net](mailto:olivia@oliviamaia.net)  
→  [grupo *rabiscologia* no telegram](https://t.me/joinchat/AisPeBK9IlhYdKVU4fg9Ww)

→  apoie meu trabalho!
   - [catarse](https://catarse.me/oliviamaia)
   - [liberapay](https://pt.liberapay.com/nyex/)

[mastodon_pt](https://masto.donte.com.br/@olivia) | [mastodon_en](https://eldritch.cafe/@olivia)  
[telegram](https://www.t.me/oliviamaia) | [matrix](https://matrix.to/#/@nyex:matrix.org)  
[vídeos](https://share.tube/accounts/olivia/) | [lives](https://live.oliviamaia.net) | [codeberg](https://codeberg.org)

caixa postal 27  
lençóis - bahia  
46960-000  

---

você recebeu esta mensagem porque seu e-mail está cadastrado em minha newsletter. para **CANCELAR SUA ASSINATURA**, basta responder a este e-mail com seu pedido de cancelamento.

(se você **já fez isso** e eu ainda assim não removi seu e-mail a culpa é toda minha. me avisa que eu juro que resolvo dessa vez.)
