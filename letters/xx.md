---
title: "pequeninas alegriazinhas literárias"
date: 2020-08-02 20:00:00
---

> El primero que despertará será el Pez. (...) Después, irán despertando las otras formas. Gradualmente diferirán de nosotros, gradualmente no nos imitarán. Romperán las barreras de vidrio o de metal y esta vez no serán vencidas. Junto a las criaturas de los espejos combatirán las criaturas del agua.
> En el Yunnan no se habla del Pez sino del Tigre del Espejo. Otros entienden que antes de la invasión oiremos desde el fondo de los espejos el rumor de las armas.

_El libro de los Seres Imaginarios_, Jorge Luis Borges & Margarita Guerrero

essas aproximações, tão Borges. os seres dos espelhos como criaturas aprisionadas por causa de uma guerra antiga, esquecida. que é dar às coisas cotidianas um rumor distante de algo desconhecido. passar pelo espelho pela manhã pra escovar os dentes e escutar um rumor de armas. talvez seja o seu vizinho com o barbeador elétrico. você tem certeza?

ninguém está a salvo. inevitável que se levantem, a começar pelos peixes, ou pelo tigre do espelho. algo se organiza invisível à nossa percepção.

isso: lemberar que o mundo está um pouco além do que se vê. que o impossível se faz nas entrelinhas do possível. que a literatura está também no banheiro quando você começa o dia penteando os cabelos. será?

---

você tem uma alegriazinha literária? um trecho querido, que faz pensar que o mundo é enorme, a literatura é gigante. aquele pedacinho de livro que faz as coisas se encaixarem.
q
manda pra mim?

![assinatura](https://oliviamaia.net/img/assinatura.png)  
**olivia maia**  
_escritora e ilustradora desterrada. provavelmente comunista._

→ [olivia@oliviamaia.net](mailto:olivia@oliviamaia.net)  
→ [grupo *rabiscologia* no telegram](https://t.me/joinchat/AisPeBK9IlhYdKVU4fg9Ww)

[mastodon](https://radical.town/@olivia) | [telegram](https://www.t.me/oliviamaia) | [matrix](https://matrix.to/#/@nyex:matrix.org)  
[twitch](https://twitch.tv/shadnyex) | [gitlab](https://gitlab.com/oliviamaia)

caixa postal 27  
lençóis - bahia  
46960-000  

---

você recebeu esta mensagem porque seu e-mail está cadastrado em minha newsletter. para **CANCELAR SUA ASSINATURA**, basta responder a este e-mail com seu pedido de cancelamento.

(se você **já fez isso** e eu ainda assim não removi seu e-mail a culpa é toda minha. me avisa que eu juro que resolvo dessa vez.)
