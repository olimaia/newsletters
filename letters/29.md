---
title: "uma mensagem na garrafa"
date: 2020-07-31 22:00:00
---

eu escrevi [isso aqui](https://oliviamaia.net/2020/por-ai-internet-afora/), mas acho que ninguém leu. 

às vezes eu escrevo umas coisas.

e publiquei [várias novas ilustrações](https://oliviamaia.net/arte/rabiscos) no site também.

pensei em parar de mandar newsletter mas depois pensei de novo. 10 pessoas lendo ainda são 10 pessoas lendo. e eu gosto de vocês, minhas queridas 10 pessoas lendo.

ando querendo voltar pro mundo da literatura. depressão é uma merda. passou o tempo que dava pra fazer alguma coisa com ela, alguma artezinha, um [zine](https://oliviamaia.net/zine/#rabiscologia_007) ou [dois](https://oliviamaia.net/zine/#rabiscologia_008). depressão não tem graça.

tenho feito ilustração ao vivo todos os domingos no [twitch](https://twitch.tv/shadnyex). em inglês, por enquanto. se tiver interesse, faço em português também. por enquanto não teve.

eu só tenho conseguido desenhar assim, enquanto converso ao vivo com os amigos. porque me obrigo a estar ali. tão difícil estar em qualquer lugar.

me conta de você. tem lido o quê? ou nada? altos trabalhos via videoconferência? escrevendo alguma coisa, desenhando alguma coisa, cuidando de plantinhas? manda foto das plantinhas.

vem no [grupo do telegram](https://t.me/joinchat/AisPeBK9IlhYdKVU4fg9Ww). às vezes tem umas fotos de gatos. é legal.

![assinatura](https://oliviamaia.net/img/assinatura.png)  
**olivia maia**  
_escritora e ilustradora desterrada. provavelmente comunista._

[olivia@oliviamaia.net](mailto:olivia@oliviamaia.net)  
[mastodon](https://radical.town/@olivia) | [streaming](https://twitch.tv/shadnyex) | [gitlab](https://gitlab.com/oliviamaia)

caixa postal 27  
lençóis - bahia  
46960-000  
