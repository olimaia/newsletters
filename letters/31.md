---
title: "as novidades todas"
date: 2020-09-28 11:00:00
---

eee tem novidades!

como estão as coisas por aí? espero que com saúde e dinheiros, que é o que tá mais complicado nesses tempos.

primeiro de tudo a **novidade super ultra master linda**:

minha nova [campanha de assinatura recorrente no CATARSE](https://catarse.me/oliviamaia). agora você pode apoiar meu trabalho e ganhar uns presentinhos eventuais. as recompensas não são assim GRANDES COISA mas eu quero continuar disponibilizando meu trabalho e artes DE GRAÇA PRA TODO MUNDO sempre que possível, assinantes ou não.

por isso se você puder apoiar essa autora e ilustradora independente que acredita em cultura livre e acessível, toda minha gratidão pra você <3 se não puder apoiar com dinheiros peço uma forcinha na divulgação :)

chega lá no link e dá uma espiada no projeto que eu explico tudo que tenho feito, como tenho feito, e porque eu não quero fazer coisas exclusivas pra assinantes. de qualquer forma, sim, tem uns presentinhos pra fazer coleção!

que tal que tal? estou aqui tentando fazer coisas e artes e hortas e plantas nessa leseira de cidade turística sem turista. não é fácil, mas a gente tenta. e com companhia é sempre melhor. por isso a campanha é um convite: vem me fazer companhia? já tem DOIS apoiadores no momento em que escrevo este e-mail. TAMO CHIQUE.

também queria compartilhar outras novidades e coisas:

 - fiz uma [página de originais](https://oliviamaia.net/arte/originais) disponíveis pra quem tiver interesse em uma arte original e chique e você paga quando quiser e puder. basta cobrir os custos de envio.
 - tem mais ilustrações novas na [página de ilustrações](https://oliviamaia.net/arte/rabiscos) tudo livre pra baixar em alta resolução de graça e com amor.
 - fiz uma [versão reduzida do meu site em inglês](https://oliviamaia.net/en) principalmente com as artes pra quem quiser compartilhar cos amigo gringo :)
 - dei uma pausa nos streamings/lives de ilustração mas já estou de volta, todo domingo e algumas sextas-feiras, por volta de 10h-11h no [twitch](https://twitch.tv/shadnyex). não precisa ter conta pra assistir às lives mas se quiser participar do chat (cola no chat!) aí precisa. e com conta você pode me seguir e receber notificação quando estiver ao vivo.
 - andei escrevendo um pouco mais do NORTE, meu [romance aberto](https://norte.oliviamaia.net) que você pode acompanhar durante o processo de escrita.
 - o livro SHERLOCK HOLMES: O JOGO CONTINUA da editora Draco foi [lançado em versão impressa](https://editoradraco.com/produto/sherlock-holmes2/) com cheiro de livro depois de uma campanha bem sucedida no catarse e tem um conto meu "Notas sobre a aventura do capitão ausente". estou aqui ansiosamente esperando meu exemplar chegar.
 - fiz um [canal no peertube](https://share.tube/accounts/olivia/) (plataforma descentralizada de vídeos tipo youtube mas sem algoritmo e google) e você pode assinar com qualquer conta do fediverso ou via rss. tem dois vídeos novos de ilustrações em time-lapse (tipo ritmo acelerado) e HOJE MESMO vou editar e publicar mais TRÊS. cola lá que tá bonito.

era isso por hoje. estou aqui tentando também começar um projeto de podcast de episódios ultra breves mas essa coisa de falar no microfone (e pra câmera porque o podcast vai ter versão sem cortes publicada no meu canal de vídeo) ainda tá meio enrolada na minha cabeça. eu chego lá. foo.

vou ali editar uns vídeos. dá uns gritos aí e me conta que tem feito?

abraço!

![assinatura](https://oliviamaia.net/img/assinatura.png)  
**olivia maia**  
_escritora e ilustradora desterrada. provavelmente comunista._

→ [olivia@oliviamaia.net](mailto:olivia@oliviamaia.net)  
→ [grupo *rabiscologia* no telegram](https://t.me/joinchat/AisPeBK9IlhYdKVU4fg9Ww)

→ [apoie meu trabalho!](https://catarse.me/oliviamaia)

[mastodon](https://radical.town/@olivia) | [telegram](https://www.t.me/oliviamaia) | [matrix](https://matrix.to/#/@nyex:matrix.org)  
[twitch](https://twitch.tv/shadnyex) | [gitlab](https://gitlab.com/oliviamaia)

caixa postal 27  
lençóis - bahia  
46960-000  

---

você recebeu esta mensagem porque seu e-mail está cadastrado em minha newsletter. para **CANCELAR SUA ASSINATURA**, basta responder a este e-mail com seu pedido de cancelamento.

(se você **já fez isso** e eu ainda assim não removi seu e-mail a culpa é toda minha. me avisa que eu juro que resolvo dessa vez.)
